Image Utilities
======================

> Provides developer-oriented improvements to the core image module.

## Features
- Add `image_style` Twig filter. It takes field items, media or file entities
and gives you an image url based on the image style ID you pass as an argument.
- Add `ImageManager` service that can be used to easily generate image urls
in code.
- Add `getFile`, `getAlt` and `getTitle` methods to the image field item class
for use in custom code or Twig templates.

## Installation
This package requires PHP 7.2 and Drupal 8 or higher. It can be installed using
 Composer:

```bash
 composer require drupal/image_utilities
```

## Contributing
- Tests are encouraged. This project doesn't have any test coverage yet,
 but contributions are welcome.
- Keep the documentation up to date. Make sure README.md and other relevant
 documentation is kept up to date with your changes.
- One pull request per feature. Try to keep your changes focused on solving a
 single problem. This will make it easier for us to review the change and easier
 for you to make sure you have updated the necessary tests and documentation.

## Changelog
All notable changes to this project will be documented in the
[CHANGELOG](CHANGELOG.md) file.
