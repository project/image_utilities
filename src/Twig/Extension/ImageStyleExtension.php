<?php

namespace Drupal\image_utilities\Twig\Extension;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\image_utilities\ImageManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * A Twig extending for building image urls.
 */
class ImageStyleExtension extends AbstractExtension {

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The image manager.
   *
   * @var \Drupal\image_utilities\ImageManagerInterface
   */
  protected $imageManager;

  /**
   * ImageStyleExtension constructor.
   *
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator.
   * @param \Drupal\image_utilities\ImageManagerInterface $imageManager
   *   The image manager.
   */
  public function __construct(
    FileUrlGeneratorInterface $fileUrlGenerator,
    ImageManagerInterface $imageManager
  ) {
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->imageManager = $imageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    return [
      new TwigFilter('image_style', [$this, 'applyImageStyle']),
    ];
  }

  /**
   * Get the image url for the incoming file object, with a certain image style.
   */
  public function applyImageStyle($file, string $style, bool $relative = TRUE, bool $directUrlIfNotSupported = FALSE): ?string {
    // @todo Dispatch cache metadata
    $url = $this->imageManager->getImageUrlByAnyObject($file, $style, $directUrlIfNotSupported);

    if ($url && $relative) {
      return $this->fileUrlGenerator->transformRelative($url);
    }

    return $url;
  }

}
