<?php

namespace Drupal\image_utilities;

use Drupal\file\FileInterface;

/**
 * The image manager.
 *
 * Can be used for building image urls for certain file objects
 * and image styles.
 */
interface ImageManagerInterface {

  /**
   * Get an image url for a file entity and a certain image style.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   * @param string $imageStyleId
   *   The image style ID.
   * @param bool $directUrlIfNotSupported
   *   Whether a direct url to the file should be returned in case the
   *   image style does not support this kind of image.
   *
   * @return string|null
   *   An image url or NULL if the image is not supported
   *   and $directUrlIfNotSupported if FALSE.
   *
   * @throws \Drupal\image_utilities\Exception\ImageStyleNotFoundException
   *   In case no image style with this ID exists.
   */
  public function getImageUrl(FileInterface $file, string $imageStyleId, bool $directUrlIfNotSupported = FALSE): ?string;

  /**
   * Get an image url for a file-ish object and a certain image style.
   *
   * @param \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem|\Drupal\image_utilities\Plugin\Field\FieldType\ImageItem|\Drupal\media\MediaInterface $file
   *   A file-ish object.
   * @param string $imageStyleId
   *   The image style ID.
   * @param bool $directUrlIfNotSupported
   *   Whether a direct url to the file should be returned in case the
   *   image style does not support this kind of image.
   *
   * @return string|null
   *   An image url or NULL if the image is not supported
   *   and $directUrlIfNotSupported if FALSE,
   *   or in case the file-ish object does not resolve to a file.
   *
   * @throws \Drupal\image_utilities\Exception\ImageStyleNotFoundException
   *   In case no image style with this ID exists.
   */
  public function getImageUrlByAnyObject($file, string $imageStyleId, bool $directUrlIfNotSupported = FALSE): ?string;

}
