<?php

namespace Drupal\image_utilities;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\file\FileInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\image_utilities\Exception\ImageStyleNotFoundException;
use Drupal\media\MediaInterface;

/**
 * The image manager.
 *
 * Can be used for building image urls for certain file objects
 * and image styles.
 */
class ImageManager implements ImageManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface]
   */
  protected $entityTypeManager;

  /**
   * ImageManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrl(FileInterface $file, string $imageStyleId, bool $directUrlIfNotSupported = FALSE): ?string {
    $storage = $this->entityTypeManager->getStorage('image_style');

    if (!$imageStyle = $storage->load($imageStyleId)) {
      throw new ImageStyleNotFoundException($imageStyleId);
    }

    $path = $file->getFileUri();

    if (!$imageStyle->supportsUri($path)) {
      if ($directUrlIfNotSupported) {
        return $file->createFileUrl(FALSE);
      }

      return NULL;
    }

    return $imageStyle->buildUrl($path);
  }

  /**
   * {@inheritdoc}
   */
  public function getImageUrlByAnyObject($file, string $imageStyleId, bool $directUrlIfNotSupported = FALSE): ?string {
    if ($file instanceof EntityReferenceItem) {
      $file = $file->get('entity')->getValue();
    }

    if ($file instanceof MediaInterface) {
      $source = $file->getSource();
      $field = $source->getConfiguration()['source_field'] ?? NULL;

      if ($file->get($field)->entity) {
        $file = $file->get($field)->entity;
      }
    }

    if ($file instanceof ImageItem) {
      $file = $file->get('entity')->getValue();
    }

    if (!$file instanceof FileInterface) {
      return NULL;
    }

    return $this->getImageUrl($file, $imageStyleId, $directUrlIfNotSupported);
  }

}
