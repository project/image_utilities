<?php

namespace Drupal\image_utilities\Plugin\Field\FieldType;

use Drupal\file\FileInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem as ImageItemBase;

/**
 * A field item class with more getters than the default image field item class.
 */
class ImageItem extends ImageItemBase {

  /**
   * Get the file entity.
   */
  public function getFile(): ?FileInterface {
    return $this->get('entity')->getValue();
  }

  /**
   * Get the alternative image text, for the image's 'alt' attribute.
   */
  public function getAlt(): ?string {
    return $this->get('alt')->getValue();
  }

  /**
   * Get the title text, for the image's 'title' attribute.
   */
  public function getTitle(): ?string {
    return $this->get('title')->getValue();
  }

}
